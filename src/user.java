/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Supawee
 */
public class user {
    String name;
    String surname;
    String username;
    String password;
    String tel;
    double weight;

    public user() {
    }

    public user(String name, String surname, String username, String password, String tel, double weight, double height) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.tel = tel;
        this.weight = weight;
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getTel() {
        return tel;
    }

    public double getWeight() {
        return weight;
    }

    public double getHeight() {
        return height;
    }
    double height;
    
}
