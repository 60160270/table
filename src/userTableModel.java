
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Supawee
 */
public class userTableModel extends AbstractTableModel {

    String[] columnName = {"name", "surname", "username", "password"};
    ArrayList<user> user_list = data.user_list;

    public String getColumnCount(int column) {
        return columnName[column];
    }

    @Override
    public int getRowCount() {
        return user_list.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        user user = user_list.get(rowIndex);
        if (user == null) {
            return "";
        }
        switch (columnIndex) {
            case 0:
                return user.getName();
            case 1:
                return user.getSurname();
            case 2:
                return user.getUsername();
            case 3:
                return user.getPassword();
        }
        return "";
    }

    @Override
    public int getColumnCount() {
        return columnName.length;
    }

}
